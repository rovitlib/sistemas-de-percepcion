#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <boost/algorithm/string.hpp>
#include <math.h>


using namespace std;




void spawnViewer(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->addCoordinateSystem (1.0);
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(pointCloud);
    viewer->addPointCloud<pcl::PointXYZRGBA> (pointCloud, rgb, "id0");
    while(!viewer->wasStopped ()) {
    	viewer->removePointCloud("id0");
    	viewer->addPointCloud<pcl::PointXYZRGBA> (pointCloud, rgb, "id0");
		viewer->spinOnce(100);
	}
	viewer->close();
}





int main (int argc, char** argv)
{

	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
	boost::thread visualizer = boost::thread(spawnViewer, pointCloud);

	cout << "Main thread waiting 5 seconds to proceed" << endl;
	boost::this_thread::sleep(boost::posix_time::seconds(5));

	
	pcl::io::loadPCDFile<pcl::PointXYZRGBA> ("../scenes/cloud_291.pcd", *pointCloud);


	cout << "Main thread waiting 5 seconds to proceed" << endl;
	boost::this_thread::sleep(boost::posix_time::seconds(5));

	
	pcl::io::loadPCDFile<pcl::PointXYZRGBA> ("../scenes/cloud_294.pcd", *pointCloud);


	cout << "Main thread waiting 5 seconds to proceed" << endl;
	boost::this_thread::sleep(boost::posix_time::seconds(5));

	
	pcl::io::loadPCDFile<pcl::PointXYZRGBA> ("../scenes/cloud_291.pcd", *pointCloud);

	cout << "Main thread waiting 5 seconds to proceed" << endl;
	boost::this_thread::sleep(boost::posix_time::seconds(5));

	
	pcl::io::loadPCDFile<pcl::PointXYZRGBA> ("../scenes/cloud_294.pcd", *pointCloud);







	visualizer.join();


	return 0;
}
