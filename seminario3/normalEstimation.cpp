#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>

#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/filter.h>

#include <boost/thread/thread.hpp>

using namespace std;
using namespace pcl;

int main (int argc, char** argv) {
  string pcdFile;

  //Comprobar si se ha recibido parámetro
  if(argc > 1) {
    //Nombre de fichero introducido
    pcdFile = string(argv[1]);
  }
  else {
    // Escena de Kinect
    pcdFile = "../scenes/xbox.pcd";
  }

  // Cargar una nube de puntos
  PointCloud<PointXYZ>::Ptr cloud (new PointCloud<PointXYZ>);
  if (io::loadPCDFile<PointXYZ> (pcdFile, *cloud) == -1) {
    cerr << "Error al cargar la nube de puntos " + pcdFile + "\n" << endl;
    return -1;
  }

  // Eliminar los valores NaN
  vector<int> indices;
  removeNaNFromPointCloud(*cloud, *cloud, indices);

  // Crear la clase de estimacion de normales y establecer la nube de puntos de entrada
  NormalEstimation<PointXYZ, Normal> ne;
  ne.setInputCloud(cloud);

  // Calcular el centroide de la nube de puntos
  Eigen::Vector4f centroid;
  compute3DCentroid (*cloud, centroid);
  
  // Establecer el punto de vista en el centroide de la nube de puntos
  ne.setViewPoint(centroid[0], centroid[1], centroid[2]);

  // Crear una representacion kdtree vacia y pasarle el objeto de estimacion de normales.
  // Su contenido se rellenara dentro del objeto, basandose en la nube de puntos de entrada dada.
  search::KdTree<PointXYZ>::Ptr tree (new search::KdTree<PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  PointCloud<Normal>::Ptr cloud_normals (new PointCloud<Normal>);

  // Usar todos los vecinos de una esfera de radio 30
  ne.setRadiusSearch (30);

  // Usar 10 vecinos
  // ne.setKSearch(10); 

  // Calcular las normales
  ne.compute(*cloud_normals);

  // Mezclar las normales con los puntos
  PointCloud<PointNormal>::Ptr cloud_with_normals (new PointCloud<PointNormal>);
  concatenateFields(*cloud, *cloud_normals, *cloud_with_normals);

  // Eliminar las normales NaN y los outliers de la nube de puntos
  PointCloud<PointNormal>::Ptr temp_filtered_cloud (new PointCloud<PointNormal>);
  indices.clear();
  removeNaNNormalsFromPointCloud(*cloud_with_normals, *temp_filtered_cloud, indices);
  copyPointCloud(*temp_filtered_cloud, *cloud_with_normals);

  // Corregir la direccion de las normales
  for (int i =0; i < cloud_with_normals->points.size(); i++) {
    cloud_with_normals->points[i].normal_x = -cloud_with_normals->points[i].normal_x;
    cloud_with_normals->points[i].normal_y = -cloud_with_normals->points[i].normal_y;
    cloud_with_normals->points[i].normal_z = -cloud_with_normals->points[i].normal_z;

    //flipNormalTowardsViewpoint(cloud_with_normals->points[i],0,0,1,cloud_with_normals->points[i].normal_x,cloud_with_normals->points[i].normal_y,cloud_with_normals->points[i].normal_z); 
  }

  // Visualizar las normales
  boost::shared_ptr<visualization::PCLVisualizer> viewer (new visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0.0, 0.0, 0.0);
  viewer->addPointCloud<pcl::PointNormal>(cloud_with_normals, "cloud");
  viewer->addPointCloudNormals<pcl::PointNormal>(cloud_with_normals,1,10,"normals");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "normals");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.4, 0.0, "normals");
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();

  /*viewer->setBackgroundColor (0, 0, 0);
  viewer->addPointCloud<PointXYZ> (cloud, "xbox cloud");
  viewer->addPointCloudNormals<PointXYZ, Normal>(cloud, cloud_normals);
  //viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_POINT_SIZE, 1, "xbox cloud");
  //viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 1.0, 1.0, 0.0);
  viewer->setPointCloudRenderingProperties (visualization::PCL_VISUALIZER_LINE_WIDTH, 3); 

  viewer->addCoordinateSystem (1.0);
  //viewer->initCameraParameters (); 
*/

  while (!viewer->wasStopped ()) {
    viewer->spinOnce(100);
    boost::this_thread::sleep(boost::posix_time::microseconds(100000));
  }

  return 0;
}
