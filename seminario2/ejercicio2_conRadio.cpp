#include <iostream>
#include <vector>
#include <ctime>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/filters/filter.h>

int main (int argc, char** argv) {
		
     std::string pcdFile1;
     std::string pcdFile2;

	//Comprobar si se ha recibido par�metros
	if(argc > 2)
	{
		//Nombre de fichero introducido
		pcdFile1 = std::string(argv[1]);
		pcdFile2 = std::string(argv[2]);
	}
	else
	{
		// Escena de Kinect
		pcdFile1 = "../scenes/cloud_291.pcd";
		pcdFile2 = "../scenes/cloud_292.pcd";
	}

	// Cargar una nube de puntos
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud1 (new pcl::PointCloud<pcl::PointXYZ>);
	if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile1, *pointCloud1) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile1+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices; 
        pcl::removeNaNFromPointCloud(*pointCloud1, *pointCloud1, indices); 

        pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud2 (new 	pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile2, *pointCloud2) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile2+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices2; 
        pcl::removeNaNFromPointCloud(*pointCloud2, *pointCloud2, indices2); 

 	// Indicamos el n�mero de vecinos que usaremos en la busqueda
 	int nvecinos = 1;
 	float radius = 10;

 	int npuntos = 0;
 	float distancia = 0.0;

 	pcl::PointXYZ searchPoint;

 	// creamos una instancia del kdtree
 	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;

 	// asignamos la nube de puntos y construimos el arbol
 	kdtree.setInputCloud (pointCloud2);

	// Para cada punto de pointCloud1 buscamos su vecino m�s pr�ximo en pointCloud2
 	for (size_t i = 0; i < pointCloud1->points.size (); ++i) {
        // Definimos el punto de b�squeda
	   searchPoint.x = pointCloud1->points[i].x;
	   searchPoint.y = pointCloud1->points[i].y;
	   searchPoint.z = pointCloud1->points[i].z;

 	   // B�squeda de los K vecinos m�s pr�ximos en un radio determinado
 	   std::vector<int> pointIdxRadiusSearch;
 	   std::vector<float> pointRadiusSquaredDistance;

 	   if ( kdtree.radiusSearch (searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0 ) {
 	     // Se ha encontrado un punto
 	     distancia += pointRadiusSquaredDistance[0];
 	     npuntos++;
 	
             std::cout << "Distancia: " << pointRadiusSquaredDistance[0] << std::endl;
 	   }
 	}

 	std::cout << "Distancia media entre las dos nubes de puntos: " << distancia / npuntos << std::endl;

	return 0;
}
