#include <iostream>
#include <vector>
#include <ctime>
#include <cmath>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/filters/filter.h>
#include <pcl/common/geometry.h>

int main (int argc, char** argv) {
		
     std::string pcdFile1;
     std::string pcdFile2;

	//Comprobar si se ha recibido parámetros
	if(argc > 2)
	{
		//Nombre de fichero introducido
		pcdFile1 = std::string(argv[1]);
		pcdFile2 = std::string(argv[2]);
	}
	else
	{
		// Escena de Kinect
		pcdFile1 = "../scenes/cloud_291.pcd";
		pcdFile2 = "../scenes/cloud_292.pcd";
	}

	// Cargar una nube de puntos
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud1 (new pcl::PointCloud<pcl::PointXYZ>);
	if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile1, *pointCloud1) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile1+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices; 
        pcl::removeNaNFromPointCloud(*pointCloud1, *pointCloud1, indices); 

        pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud2 (new 	pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile2, *pointCloud2) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile2+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices2; 
        pcl::removeNaNFromPointCloud(*pointCloud2, *pointCloud2, indices2); 

 	int npuntos = 0;
 	float distancia = 0.0;

        double distPto = 0.0;
        double distMin = 0.0;

 	pcl::PointXYZ searchPoint;
        pcl::PointXYZ cloudPoint;

	// Para cada punto de pointCloud1 buscamos su vecino más próximo en pointCloud2
 	for (size_t i = 0; i < pointCloud1->points.size (); ++i) {
        // Definimos el punto de búsqueda
	   searchPoint.x = pointCloud1->points[i].x;
	   searchPoint.y = pointCloud1->points[i].y;
	   searchPoint.z = pointCloud1->points[i].z;

           distPto = 0.0;
           distMin = 9999999999999999999999.9;

 	   for (size_t j = 0; j < pointCloud2->points.size (); ++j) {
              cloudPoint.x = pointCloud2->points[j].x;
              cloudPoint.y = pointCloud2->points[j].y;
              cloudPoint.z = pointCloud2->points[j].z;

              //distPto = sqrt( pow((searchPoint.x - cloudPoint.x), 2.0) + pow((searchPoint.y - cloudPoint.y), 2.0) + pow((searchPoint.z - cloudPoint.z), 2.0) );

              distPto = pcl::geometry::distance(searchPoint, cloudPoint);

              if (distPto < distMin) {
                 distMin = distPto;
              }
           }

 	   distancia += distMin;
 	   npuntos++;
 	}

 	std::cout << "Distancia media entre las dos nubes de puntos: " << distancia / npuntos << std::endl;

	return 0;
}
