#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>

#include <pcl/filters/filter.h>

using namespace pcl;

int main (int argc, char** argv) {
		
	std::string pcdFile1;
     std::string pcdFile2;

	//Comprobar si se ha recibido par�metros
	if(argc > 2)
	{
		//Nombre de fichero introducido
		pcdFile1 = std::string(argv[1]);
		pcdFile2 = std::string(argv[2]);
	}
	else
	{
		// Escena de Kinect
		pcdFile1 = "../scenes/cloud_291.pcd";
		pcdFile2 = "../scenes/cloud_292.pcd";
	}

	// Cargar una nube de puntos
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud1 (new pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile1, *pointCloud1) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile1+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices; 
        pcl::removeNaNFromPointCloud(*pointCloud1, *pointCloud1, indices); 

	pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud2 (new 	pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile2, *pointCloud2) == -1)
	{
		std::cerr<<"Error al cargar la nube de puntos "+pcdFile2+"\n"<<std::endl;
		return -1;
	}

        // Remove NaN values
        std::vector<int> indices2; 
        pcl::removeNaNFromPointCloud(*pointCloud2, *pointCloud2, indices2); 

	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
      // Establecemos la nube de puntos de partida
	icp.setInputCloud(pointCloud1);

     // Indicamos la nube de puntos final
	icp.setInputTarget(pointCloud2);

     // Definimos la nube de puntos donde se almacenara el resultado estimado
	pcl::PointCloud<pcl::PointXYZ> pointCloudAligned;

     // Aplicamos el algoritmo de alineamiento
	icp.align(pointCloudAligned);

     // Mostramos por pantalla si ha convergido (= 1 indica que es posible pasar de pointCloud1 a pointCloud2 mediante una transformaci�n r�gida, as� como el Euclidean fitness score a partir de las distancias entre los puntos de origen y los de salida
	std::cout << "has converged:" << icp.hasConverged() << " score: " << icp.getFitnessScore() << std::endl;

     // Mostramos la transformaci�n que aline� pointCloud1 con pointCloudAligned
	std::cout << icp.getFinalTransformation() << std::endl;

	return 0;
}
