import numpy as np
import cv2
import glob
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
# grids are 7x6, so a set of these 3D points is going to be detected in each color frame
# of the chessboard
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

# Load the chessboard images
images = glob.glob('images/*.jpg')

# for each image
for fname in images:

    # read the image and convert it to grayscale
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (7,6),None)

    # If found, add 3D object points and 2D image points 
    if ret == True:
        objpoints.append(objp)

        # bonus: in order to improve the corner detection, a subpixel algorithm is applied
        corners = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners)

        # Draw and display the corners. Only for visualization purposes
        img = cv2.drawChessboardCorners(img, (7,6), corners,ret)
        cv2.imshow('img',img)
        cv2.waitKey(500)
    else:
        print(fname+" cannot recognize the pattern")

cv2.destroyAllWindows()

# in this point we have a bunch of 2D and 3D correspondences extracted from the images
# now go for the calibration itself
ret, K, dist, R, t = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
print("Intrinsic Parameters")
print(K)
# for each image (pose of the camera) we have a pair of rotation and translation vectors
print("Rotation Matrices")
print(np.array(R).shape)
print(np.array(R))
print("Translation Matrices")
print(np.array(t).shape)
print(np.array(t))




# visualizing the points where the camera took each image
# note that no rotations is applied, so only the position is presented here
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(len(t)):
    ax.scatter(t[i][0][0], t[i][1][0], t[i][2][0])
ax.plot((0,1),(0,0),(0,0))
ax.plot((0,0),(0,1),(0,0))
ax.plot((0,0),(0,0),(0,1))
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)
ax.set_zlim(0, 20)
plt.show()



# the pinhole camera model does not cointain a lens, so no radial distortion is considered
# nonetheless, actual cameras have a lens and radial distortion
# alongside the K, R and t, a 'dist' vector came up out of the calibration procedure, which
# contains the distortion coefficients
img = cv2.cvtColor(cv2.imread("images/left14.jpg") ,cv2.COLOR_BGR2GRAY)
dst = cv2.undistort(img, K, dist, None)

cv2.imshow("img1",img)
cv2.imshow("img2",dst)
cv2.waitKey(0)

# Exercise 1:
# Note that the undistorted image is smaller that the original image.
# Well, take a look again. The undistorted image is sort of zoomed in, 
# the outer parts of the original image was croped out in the undistorted image.
# Why is happening this?